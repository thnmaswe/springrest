package de.ohmhs.efi.maswe.springrest.service;

import de.ohmhs.efi.maswe.springrest.model.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Integer> {
    @RestResource(path = "department")
    List<Employee> findByDepartmentDepartmentId(Integer id);
}
