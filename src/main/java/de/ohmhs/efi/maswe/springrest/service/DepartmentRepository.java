package de.ohmhs.efi.maswe.springrest.service;

import de.ohmhs.efi.maswe.springrest.model.Department;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DepartmentRepository extends PagingAndSortingRepository<Department, Integer> {
}
