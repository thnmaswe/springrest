package de.ohmhs.efi.maswe.springrest.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity(name="job")
@Table(name="JOBS")
@Getter
@Setter
public class Job {

	@Id
	@Column(name="JOB_ID", length=10)
	private String jobId;
	@Column(name="JOB_TITLE", length=35, nullable=false)
	private String jobTitle;
	@Column(name="MIN_SALARY",scale=6, columnDefinition = "numeric(6,0)")
	private Integer minSalary;
	@Column(name="MAX_SALARY",scale=6, columnDefinition = "numeric(6,0)")
	private Integer maxSalary;
	
	
}
