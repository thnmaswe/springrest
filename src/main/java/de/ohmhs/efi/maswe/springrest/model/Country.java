package de.ohmhs.efi.maswe.springrest.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity(name="country")
@Table(name="COUNTRIES")
@Getter
@Setter
public class Country {
	
	@Id
	@Column(name="COUNTRY_ID",length=2, columnDefinition = "bpchar(2)")
	private String countryId;
	
	@Column(name="COUNTRY_NAME", length=40)
	private String countryName;
	
	@ManyToOne
	@JoinColumn(name="REGION_ID", referencedColumnName="REGION_ID")
	private Region region;
}