package de.ohmhs.efi.maswe.springrest.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * JPA Entity for the EMPLOYEES database table. Annotations in this entity map object
 * properties to database table and columns. See the comments in source code for explanations.
 * Further this object has Bean Validation annotations on method signatures for web-based
 * validation
 * @author Matthias Roth
 *
 */
// marks this object as JPA entity and give it an explicit name for EJBQL queries
@Entity(name="employee")
// map this object to EMPLOYEES table
@Table(name="EMPLOYEES")
@Getter
@Setter
public class Employee {

	// marks this field as primary key
	@Id
	// this id is generated and comes from a sequence
	@GeneratedValue(generator="EMPLOYEES_SEQ_GEN",strategy=GenerationType.SEQUENCE)
	// definition of the database sequence and its properties
	@SequenceGenerator(name="EMPLOYEES_SEQ_GEN",sequenceName="EMPLOYEES_SEQ", allocationSize=1)
	// column name and size of NUMBER datatype
	@Column(name="EMPLOYEE_ID",scale=6, columnDefinition = "numeric(6,0)")
	private Integer employeeId;

	// column name and size of VARCHAR2 datatype
	@Column(name="FIRST_NAME", length=20)
	private String firstName;

	@Column(name="LAST_NAME", length=25)
	private String lastName;
	
	// column name and size of VARCHAR2 datatype. give it a unique index and mark it as NOT NULL
	@Column(name="EMAIL", length=25, nullable=false, unique=true)
	private String email;
	
	// column name and size of VARCHAR2 datatype
	@Column(name="PHONE_NUMBER", length=20)
	private String phoneNumber;
	
	// column name and mark it as NOT NULL
	@Column(name="HIRE_DATE", nullable=false)
	// mapping of Java Date and SQL Date type
	@Temporal(TemporalType.DATE)
	private Date hireDate;
	
	// column name and floating point precision in database
	@Column(name="SALARY", precision=2, columnDefinition = "numeric(8,2)")
	private Float salary;
	
	// this is a relationship to an entity. Here it is self referential
	@ManyToOne
	// explicit definition of column names in owning and foreign side of this association
	@JoinColumn(name="MANAGER_ID", referencedColumnName="EMPLOYEE_ID")
	private Employee manager;
	
	// this is a relationship to an entity.
	@ManyToOne
	// explicit definition of column names in owning and foreign side of this association
	@JoinColumn(name="DEPARTMENT_ID", referencedColumnName="DEPARTMENT_ID")
	private Department department;
	
}
