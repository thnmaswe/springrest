package de.ohmhs.efi.maswe.springrest.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity(name="region")
@Table(name="REGIONS")
@Getter
@Setter
public class Region {
	
	@Id
	@Column(name="REGION_ID", columnDefinition = "numeric")
	private Long regionId;

	@Column(name="REGION_NAME", length=25)
	private String name;
}